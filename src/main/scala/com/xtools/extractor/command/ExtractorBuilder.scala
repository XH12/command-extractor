package com.xtools.extractor.command

trait CommandExtractor{
  def hasFlag(name:String):Boolean
  val flagSize:Int
  def named(name:String):Option[String]
  val namedSize:Int
  def flagList():List[String]
  def namedList():List[Tuple2[String,String]]
  def sequence(index:Int):Option[String]
  def sequenceList():List[Tuple2[Int,String]]
  val sequenceSize:Int
}

trait CommandExtractor4j{
  def hasFlag(name:String):Boolean
  val flagSize:Int
  def named(name:String):String
  val namedSize:Int
  def flagList():java.util.List[String]
  def namedList():java.util.List[Tuple2[String,String]]
  def sequence(index:Int):String
  def sequenceList():java.util.List[Tuple2[java.lang.Integer,String]]
  val sequenceSize:Int
}

object CommandExtractorBuilder{
  def build(args:Array[String]): CommandExtractor = {
    val flagRegex = "--(.+)".r
    val namedRegex = "-([^:]+):(.*)".r

    val all = args.zipWithIndex;
    val flagSeparatedList = all.partition(_._1 match {
      case flagRegex(flag)=>true
      case _ => false
    })

    val flagList  = flagSeparatedList._1.map(x=>x._1.substring(2)).toList
    val nameSeparatedList = flagSeparatedList._2.partition(_._1 match {
      case namedRegex(name,value)=>true
      case _ => false
    })
    val namedList = nameSeparatedList._1.map(_._1 match {case namedRegex(name,value)=>Tuple2(name,value)}).toList
    val sequenceList = nameSeparatedList._2.map(_._1).zipWithIndex.map(x=>Tuple2(x._2,x._1)).toList
    new CommandExtractorImpl(flagList,namedList,sequenceList)
  }
}
class CommandExtractorImpl(val flagList:List[String],val namedList:List[Tuple2[String,String]],val sequenceList:List[Tuple2[Int,String]]) extends CommandExtractor{
  override def hasFlag(name: String): Boolean = flagList.find(x=>x==name).nonEmpty

  override val flagSize: Int = flagList.size

  override def named(name: String): Option[String] = namedList.find(_._1==name).map(_._2)

  override val namedSize: Int = namedList.size

  override def sequence(index: Int): Option[String] = sequenceList.find(_._1==index).map(_._2)
  override val sequenceSize: Int = sequenceList.size
}
object CommandExtractor4jBuilder{
  def build(args:List[String]):CommandExtractor4j= new CommandExtractor4jImpl(CommandExtractorBuilder.build(args.toArray))
  def build(args:Array[String]):CommandExtractor4j= new CommandExtractor4jImpl(CommandExtractorBuilder.build(args))
}
class CommandExtractor4jImpl(extractor:CommandExtractor) extends CommandExtractor4j{
  import scala.collection.JavaConverters._
  override def hasFlag(name: String): Boolean = extractor.hasFlag(name)

  override val flagSize: Int = extractor.flagSize

  override def named(name: String): String = extractor.named(name).getOrElse(null)

  override val namedSize: Int = extractor.namedSize

  override def flagList(): java.util.List[String] = extractor.flagList().asJava

  override def namedList(): java.util.List[(String, String)] = extractor.namedList().asJava

  override def sequence(index: Int): String = extractor.sequence(index).getOrElse(null)

  override def sequenceList(): java.util.List[(java.lang.Integer, String)] = extractor.sequenceList().map(x=>Tuple2(x._1.asInstanceOf[java.lang.Integer],x._2)).asJava

  override val sequenceSize: Int = extractor.sequenceSize

}

/**
  * Created by Xeth on 18/11/17.
  */
object ExtractorBuilder{
  def main(args:Array[String])={
    val xtractor = CommandExtractorBuilder.build(Array("A","B","--i","--j","-x:xxx","-y:yyy","C"))
    println(xtractor.flagSize)
    println(xtractor.namedSize)
    println(xtractor.sequenceSize)
  }

}